import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MissKateFreebie
{
    public static void main(String[] args)
    {
        WebDriver driver = new ChromeDriver();

        driver.get("http://www.misskatecuttables.com/products/freebie-of-the-day/");

        // find and select the freebie of the day item
        WebElement resultGrid = driver.findElement(By.id("result_grid"));
        WebElement resultLink = resultGrid.findElement(By.partialLinkText("Freebie of the Day!"));
        resultLink.click();

        // click the "Add to Cart" button
        WebElement addToCartButton = driver.findElement(By.id("add_to_cart_btn"));
        addToCartButton.click();

        // find and click the "Checkout" link
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("message_spot")));
        WebElement checkoutLink = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Checkout ")));
        checkoutLink.click();

        // log in
        WebElement email = driver.findElement(By.cssSelector(".inp[name=email]"));
        email.click();
        email.sendKeys("shunt00@bellsouth.net");
        WebElement password = driver.findElement(By.cssSelector(".inp[name=password]"));
        password.click();
        password.sendKeys("Eggman19");
        WebElement loginButton = driver.findElement(By.cssSelector(".login_form button[type=submit]"));
        loginButton.click();

        // click through the checkout pages
        WebElement sameAsBilling = driver.findElement(By.id("shipsame"));
        sameAsBilling.click();
        for (int i=0; i<4; ++i)
        {
            WebElement nextButton = driver.findElement(By.cssSelector(".step_action_buttons button[type=submit]"));
            nextButton.click();
        }

        // find the download link and get the file
        WebElement downloadItems = driver.findElement(By.className("download_items_box"));
        WebElement newItemLink = downloadItems.findElement(By.partialLinkText("freebie"));
        newItemLink.click();

        driver.close();
    }
}
